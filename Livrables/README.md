Bonjour, merci d'utiliser notre site. Veuillez suivre les manipulations ci-dessous pour un premier paramétrage.

Installez PGADMIN 4, connectez-y vous et créez une base de données, nommez la : libDiscount
Créez un login nommé : libraryAdmin    
Dans l'onglet "Definition", toujours dans la fenêtre de Login role, entrez le password : 1234
Dans l'onglet "Privileges" encore dans la fenêtre de Login Role, paramétrez tout sur YES

Mettez à jour la base de données CDAMessagerie en faisant clic droit -> Regénérer

Laissez PGADMIN 4 ouvert tant que vous utilisez le logiciel de boîte mail

Installez et ouvrez DBEAVER, cliquez sur l'icône de la prise tout en haut à gauche de la fenêtre,
nommé "Nouvelle connexion", sélectionnez PostgreSQL -> Suivant
Dans la fenêtre Connection Settings, saisissez dans le champs Database : libDiscount
champs Nom d'utilisateur : libraryAdmin
champs Mot de passe : 1234
Cliquez enfin sur Terminer

Sur le panneau de contrôle de gauche, déroulez libDiscount, Schema, public et laissez le logiciel DBEAVER
ouvert tant que vous utilisez le logiciel de boite mail



Double-cliquez sur le fichier finissant par .war

Si vous vous connectez en tant qu'admin pour la première fois, faites comme suit :
- Dans la page d'accueil du site, cliquez sur "Connexion", votre base de données s'initialise et peut prendre 
quelques secondes
- Retournez sur le logiciel Dbeaver, faites clic droit sur
libDiscount, cliquez sur "Éditeur SQL"
Allez sur l'onglet qui vient de s'ouvrir (doit contenir <libDiscount>), en parallèle ouvrez le fichier texte
nommé ScriptSQL.txt, faites la commande CTRL+A puis CTRL+C, retournez dans DBEAVER et faites la 
commande CTRL+V.
Sélectionnez tout le texte collé et faire la commande ALT+X
Mettez à jour la base de données CDAMessagerie en faisant F5
Votre compte est maintenant créé.
Vos login de connexion sont :

login : mohammedlib
mot de passe : libpassword

Vous pouvez maintenant retourner sur le site et vous connecter

