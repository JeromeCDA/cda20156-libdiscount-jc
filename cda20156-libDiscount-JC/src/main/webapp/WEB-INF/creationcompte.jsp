<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Cr�ation compte</title>
    <style><%@include file="/ressourcesFront/style/createaccount.css" %></style>
    </head>
<body>
    
    <header>
            <h4>libDiscount</h4>
            <nav>
                <ul id="ulstyle">
                    <li><a href="homepage.html">Home</a></li>
                    <li><a href="#">D�poser une annonce</a></li>
                    <li><a href="#">Mes annonces</a></li>
                    <li><a href="#">Chercher une r�f�rence</a></li>
                    <li><a href="#">Connexion</a></li>
                </ul>
            </nav>  
        </header>
	<form action="CreateAccountServlet", method="post">
		<div>
			<input type="text" placeholder=" " name="name" required>
			<label for="name">Nom</label>
			<br>
			
			<input type="text" placeholder=" " name="firstname" required>
			<label for="firstname">Pr�nom</label>
			<br>
			
			<input type="text" placeholder=" " name="email" required>
			<label for="email">Email</label>
			<br>
			
			<input type="text" placeholder=" " name="telephone" required>
			<label for="telephone">T�l�phone</label>
			<br>
			
			<input type="text" placeholder=" " name="libraryName" required>
			<label for="libraryName">Nom librairie</label>
			<br>
			
			<input type="text" placeholder=" " name="libraryAdress" required>
			<label for="libraryAdress">Adresse Librairie</label>
			<br>
			
			<input type="text" placeholder=" " name="libraryCodeP" required>
			<label for="libraryCodeP">Code postal Librairie</label>
			<br>
			
			<input type="text" placeholder=" " name="libraryCity" required>
			<label for="libraryCity">Ville Librairie</label>
			<br>
			
			<input type="text" placeholder=" " name="login" required>
			<label for="Login">Login</label>
			<br>
			
			<input type="text" placeholder=" " name="password" required>
			<label for="password">Mot de passe</label>
			<br>
			<button type="submit">Cr�er mon compte</button>
		</div>
	</form>
</body>
</html>