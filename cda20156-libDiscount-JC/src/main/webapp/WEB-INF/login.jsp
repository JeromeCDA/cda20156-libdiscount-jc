<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<title>Login</title>
    <style><%@include file="/ressourcesFront/style/login.css" %></style>
    </head>
    <body>
        <header>
            <h4>libDiscount</h4>
            <nav>
                <ul id="ulstyle">
                    <li><a href="homepage.html">Home</a></li>
                    <li><a href="#">D�poser une annonce</a></li>
                    <li><a href="#">Mes annonces</a></li>
                    <li><a href="#">Chercher une r�f�rence</a></li>
                    <li class="active"><a href="#">Connexion</a></li>
                </ul>
            </nav>  
        </header>

        <form action="LoginServlet", method="post">
			<div>
				<input type="text" placeholder=" " name="login" required>
				<label for="login">Login</label>
				<br>
				<input  type="text" placeholder=" " name="password" required>
				<label for="password">Mot de passe</label>
				<c:if test="${ !empty errorMessage }">
				<br>
				<br>
				<span id="errorLoginPW"><c:out value="${ errorMessage }"></c:out></span>
				</c:if>
				<br>
				<button type="submit">Connexion</button>
			</div>
		</form>

    </body>
</html>