<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Cr�ation compte</title>
    <style><%@include file="/ressourcesFront/style/NewAnnonce.css" %></style>
    </head>
<body>

    <c:set var="transferUser" value="${ transferUser }" scope="session" />
    
    <header>
            <h4>libDiscount</h4>
            <nav>
                <ul id="ulstyle">
                    <li><a href="BackToMainUI">Home</a></li>
                    <li class="active"><a href="#">D�poser une annonce</a></li>
                    <li><a href="#">Mes annonces</a></li>
                    <li><a href="#">Chercher une r�f�rence</a></li>
                    <li><a href="#">D�connexion</a></li>
                    <li><img id="imageUser" src="/ressourcesFront/images/user.png"></li>
					<li><span><c:out value="${ transferUser.login }"></c:out></span></li>
                </ul>
            </nav>  
        </header>

  	<form action="NewAnnonce", method="post" enctype="multipart/form-data" >
		<div>
			<input type="text" placeholder=" " name="title" required>
			<label for="title">Titre</label>
			<br>  
			
			<input type="text" placeholder=" " name="type" required>
			<label for="type">Type</label>
			<br>
			
			<input type="text" placeholder=" " name="level" required>
			<label for="level">Niveau</label>
			<br>
			
			<input type="text" placeholder=" " name="code" required>
			<label for="code">ISBN</label>
			<br>
			
			<input type="text" placeholder=" " name="editionHouse" required>
			<label for="editionHouse">Maison d'�dition</label>
			<br>
			
			<input type="text" placeholder=" " name="dateedition" required>
			<label for="dateedition">Date d'�dition</label>
			<br>
			
			<input type="text" placeholder=" " name="unitPrice" required>
			<label for="unitPrice">Prix unitaire</label>
			<br>
			
			<input type="text" placeholder=" " name="sale" required>
			<label for="sale">Remise</label>
			<br>
			
			<input type="text" placeholder=" " name="quantity" required>
			<label for="quantity">Quantit�</label>
			<br>
			
			<p>Ajoutez jusqu'� 3 photos</p>
			
			<input type="file" name="file" multiple="true">
			<br>
			
			<button type="submit">Cr�er l'annonce</button>
		</div>
	</form>
</body>
</html>