<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Interface</title>
<style>
<%@ include file ="/ressourcesFront/style/userInterface.css" %>
</style>
</head>
<body>
	<c:choose>
		<c:when test="${ SessionUser.role == 1 }">
		<c:set var="transferUser" value="${SessionUser}" scope="session"/>
		<c:set var="ClickOnMenu" value="${ BackToMainUI }"></c:set>
			<header>
				<h4>libDiscount</h4>
				<nav>
					<ul id="ulstyle">
						<li class="active"><a href="BackToMainUI">Main menu</a></li>
						<li><a href="#">D�poser une annonce</a></li>
						<li><a href="#">Mes annonces</a></li>
						<li><a href="#">Chercher une r�f�rence</a></li>
						<li><a href="Deconnexion">D�connexion</a></li>
						<li><img id="imageUser" src="/ressourcesFront/images/user.png"></li>
						<li><span><c:out value="${ SessionUser.login }"></c:out></span></li>
					</ul>
				</nav>
			</header>

			<div class="welcomeMess">
				<p>
					Bonjour <c:out value="${ SessionUser.login }"/>, bienvenue sur votre espace de travail
				</p>
			</div>
			<div id="wrapper">
				<a href="MainToNewAnnonce" data-field="D�posez une annonce">D�posez une annonce</a> 
			    <a href="InstanceDBServlet" data-field="Mes annonces">Mes annonces</a> 
			    <a href="InstanceDBServlet" data-field="Chercher une annonce">Chercher une annonce</a>
			</div>
		</c:when>
		<c:when test="${ SessionUser.role == 0 }">
			<header>
				<h4>libDiscount</h4>
				<nav>
					<ul id="ulstyle">
						<li class="active"><a href="${interfacePage}">Main menu</a></li>
						<li><a href="#">Annonces</a></li>
						<li><a href="#">Supprimer une annonce</a></li>
						<li><a href="#">D�sactiver un utilisateur</a></li>
						<li><a href="Deconnexion">D�connexion</a></li>
						<li><img id="imageUser"
							src="/ressourcesFront/images/admin.png"></li>
						<li><span><c:out value="${ SessionUser.login }"></c:out></span></li>
					</ul>
				</nav>
			</header>

			<div class="welcomeMess">
				<p>
					Bonjour <c:out value="${ SessionUser.login }"/>, bienvenue sur votre espace de travail. Vous avez le r�le d'administrateur.
				</p>
			</div>
			
			<div id="wrapper">
				<a href="#" data-field="Consulter les annonces">D�posezune annonce</a> 
				<a href="#" data-field="Supprimer une annonce">Mesannonces</a> 
				<a href="#"data-field="D�sactiver un utilisateur">Chercher une annonce</a>
			</div>

		</c:when>
	</c:choose>
</body>
</html>