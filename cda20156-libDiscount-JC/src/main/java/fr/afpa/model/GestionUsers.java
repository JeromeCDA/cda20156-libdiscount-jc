package fr.afpa.model;

import java.util.ArrayList;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Librairie;
import fr.afpa.beans.User;
import fr.afpa.dao.CommunicationBDD;
import fr.afpa.roles.Roles;


public class GestionUsers {
	
	
	/**
	 * Instanciation d'un Utilisateur dans la couche model prévue à cet effet. J'ai jugé pertinent de faire de la Librairie une classe et une entité persistente
	 * à part entière car une Librairie n'est pas un libraire et peut par ailleurs en contenir plusieurs.
	 * @param name
	 * @param firstName
	 * @param email
	 * @param telephone
	 * @param login
	 * @param password
	 * @param nomLibrairie
	 * @param voieAdresseLib
	 * @param codePostalLib
	 * @param villeLib
	 */
	public void instanceUserLib(String name, String firstName, String email, String telephone, String login,
			String password, String nomLibrairie, String voieAdresseLib, String codePostalLib,
			String villeLib) {
		
		
		CommunicationBDD dao = new CommunicationBDD();
		Annonce fkUser = new Annonce();
		Librairie lib = new Librairie(nomLibrairie, voieAdresseLib, codePostalLib, villeLib);
		User libraire = new User(name, firstName, email, telephone, login,
			password, Roles.USER, true);
		
		libraire.setLibrairie(lib);
//		fkUser.setUser(libraire);

		dao.createAccountDB(libraire);
	}
	
	
	/**
	 * La méthode a vocation à faire transiter la liste de user depuis la DAO jusqu'à la couche control (servlets)
	 * @param login
	 * @param password
	 * @return
	 */
	public ArrayList<User> retrieveUsersLoginDAO(String login, String password){
		CommunicationBDD dao = new CommunicationBDD();
		ArrayList<User> listUsers = dao.retrieveUsersLoginMdp(login, password);
		return listUsers;
	}
	
	

}
