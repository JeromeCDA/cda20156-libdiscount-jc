package fr.afpa.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Photo;
import fr.afpa.beans.User;
import fr.afpa.dao.CommunicationBDD;



public class GestionAnnonces {
	
	
	/**
	 * La méthode createAnnonce instancie une annonce, qui contient une liste allant de 1 à 3 photos. La méthode a besoin d'un user en paramètres car la clé étrangère de l'user est
	 * dans la table Annonces. Ici, la stratégie est d'enregistrer les images en bytes 
	 * @param titre
	 * @param type
	 * @param niveau
	 * @param isbn
	 * @param maisonEdition
	 * @param dateEdition
	 * @param prixUnitaire
	 * @param remise
	 * @param quantite
	 * @param fileParts
	 * @param user
	 */
	public void createAnnonce(String titre, String type, String niveau, String isbn,
		String maisonEdition, Date dateEdition, double prixUnitaire, int remise, int quantite,
		 List<Part>fileParts, User user) {
		CommunicationBDD annonce = new CommunicationBDD();
		Photo photos = new Photo();
		ArrayList <Photo> listPhotos = new ArrayList();
		
		if(!fileParts.isEmpty()) {	
			for (Part filePart : fileParts) {
				String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
				try {
					InputStream fileContent = filePart.getInputStream();
					byte[] image = IOUtils.toByteArray(fileContent);
					listPhotos.add(new Photo(image));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		double pxTotal = calculPrixTotal(prixUnitaire, remise, quantite);
		Annonce creatingAnnonce = new Annonce(titre, type, niveau, isbn, maisonEdition, dateEdition, prixUnitaire, remise, quantite, pxTotal, user);

	    creatingAnnonce.setListPhotos(listPhotos);
	        
	    photos.setAnnonce(creatingAnnonce);
	        
	    annonce.createAnnonceDB(creatingAnnonce);
	}
	
	
	/**
	 * Méthode qui permet le prix total à partir du prix unitaire, de la remise et de la quantité
	 * @param prixUnitaire
	 * @param remise
	 * @param quantite
	 * @return
	 */
	public double calculPrixTotal(double prixUnitaire, int remise, int quantite) {
		double total = 0.00;
		total = prixUnitaire*(1-remise/100);
		total = total*quantite;
		return total;
	}

}
