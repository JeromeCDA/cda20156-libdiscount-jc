package fr.afpa.roles;

public interface Roles {
	
	
	/**
	 * L'interface Roles facilite et factorise le système de roles pour les clients. Au lieu de créer une classe à part entière, cette alternative permet d'affecter à chaque instanciation 
	 * d'utilisateur non admin le role 1 et de le distinguer de l'admin qui a pour seul role 0. L'admin est créé à partir du script transmis via le README
	 */
	public final static int ADMIN=0;
	public final static int USER=1;

}
