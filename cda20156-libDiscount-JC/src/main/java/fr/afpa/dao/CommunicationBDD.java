package fr.afpa.dao;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.User;
import fr.afpa.session.HibernateUtils;

public class CommunicationBDD {
	
	/**
	 * Méthode qui permet d'enregistrer un user dans la DB
	 * @param user
	 */
	public void createAccountDB(User user) {
		
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();
		
		sess.save(user);
		tx.commit();
		sess.close();
	}
	
	
	/**
	 * Méthode qui permet de déterminer si les paramètres de la requête passés en arguments jusqu'à la DAO match avec ceux d'un des user de la BDD.
	 *  Retourne une ArrayList soit vide, soit avec un user
	 * @param login
	 * @param password
	 * @return
	 */
	public ArrayList<User> retrieveUsersLoginMdp(String login, String password){
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();
		
    	Query q = sess.getNamedQuery("findLoginPassword");
    	q.setParameter("login", login);
    	q.setParameter("password", password);
   
    	ArrayList <User> listUsers = (ArrayList <User>) q.getResultList();		
		
		return listUsers;
	}
	
	/**
	 * Méthode DAO qui enregistre une annonce et ses images
	 * @param annonce
	 */
	public void createAnnonceDB(Annonce annonce) {
		
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();
		
		sess.save(annonce);
		tx.commit();
		sess.close();
	}

}
