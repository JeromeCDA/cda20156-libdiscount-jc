package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;
import fr.afpa.model.GestionUsers;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	
	
	
	/**
	 * Cette méthode permet d'accéder à l'interface utilisateur ou bien admin. Si la liste de l'utilisateur est vide, c'est qu'aucun login/password
	 * n'a matché et on envoie un message d'erreur en redirigeant vers la page de login. 
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GestionUsers users = new GestionUsers();
		String message= "Login ou mot de passe incorrect";
		request.setAttribute("errorMessage", message);
		
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		ArrayList <User> listUsers = users.retrieveUsersLoginDAO(login, password);

		for(User utilisateur : listUsers) {
			if(utilisateur.getLogin().equals(login) && utilisateur.getPassword().equals(password)) {
				HttpSession sessionUser = request.getSession();
				sessionUser.setAttribute("SessionUser", utilisateur);
				request.getRequestDispatcher("/WEB-INF/userInterface.jsp").forward(request, response);
			}
		}
		
		if(listUsers.isEmpty()) {
			request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}	
			
	}

}
