package fr.afpa.servlets;

import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.model.GestionUsers;
import fr.afpa.session.HibernateUtils;

/**
 * Servlet implementation class createAccountServlet
 */
public class CreateAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	/**
	 * La méthode doPost lève des exceptions et renvoie vers des vues error si des contraintes Hibernate ne sont pas respectées
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String firstname = request.getParameter("firstname");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String libraryName = request.getParameter("libraryName");
		String libraryAdress = request.getParameter("libraryAdress");
		String libraryCodeP = request.getParameter("libraryCodeP");
		String libraryCity = request.getParameter("libraryCity");
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		
		GestionUsers toInstance = new GestionUsers();
		if(isMailOk(email) && isPhoneOk(telephone)) {
			try {
				toInstance.instanceUserLib(name, firstname, email, telephone, login, password, libraryName, libraryAdress, libraryCodeP, libraryCity);
				request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
			} catch (Exception e) {
				request.setAttribute("loginUnique", e);
				request.getRequestDispatcher("/WEB-INF/errorLoginUnique.jsp").forward(request, response);
			}
		}
		
	}
	
	/**
	 * Méthode qui permet de vérifier si l'adresse mail est au bon format
	 * @param email
	 * @return
	 */
	public boolean isMailOk(String email) {
		boolean isMailValid = false;
		String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
		if(email.matches(regex)) {
			isMailValid = true;
		}
		return isMailValid;
	}
	
	
	
	/**
	 * Méthode qui permet de vérifier si le téléphone est au bon format. La longueur du numéro est vérifiée par l'annotation @Size de Hibernate
	 * @param telephone
	 * @return
	 */
	public boolean isPhoneOk(String telephone) {
		boolean isPhoneValid = false;
		if (telephone.startsWith("06") || (telephone.startsWith("07"))) {
			isPhoneValid = true;
		}
		return isPhoneValid;
	}

}