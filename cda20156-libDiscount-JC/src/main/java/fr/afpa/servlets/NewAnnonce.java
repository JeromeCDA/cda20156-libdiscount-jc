package fr.afpa.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import fr.afpa.beans.User;
import fr.afpa.model.GestionAnnonces;

/**
 * Servlet implementation class NewAnnonce
 */

@MultipartConfig(maxFileSize = 1024 * 1024 * 10)
public class NewAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * Grâce à l'annotation @MultipartConfig, il est possible de récupérer via la requête doPost les données des images uploadées
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("transferUser");
		session.setAttribute("transferUser", user);
		String limitPhotos = "3 photos maximum par annonce";
		
		String titre = request.getParameter("title");
		String type = request.getParameter("type");
		String niveau = request.getParameter("level");
		String isbn = request.getParameter("code");
		String maisonEdition = request.getParameter("editionHouse");
		Date dateEdition=null;
		try {
			dateEdition = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dateedition"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		double prixUnitaire = Double.parseDouble(request.getParameter("unitPrice"));
		int remise = Integer.parseInt(request.getParameter("sale"));
		int quantite = Integer.parseInt(request.getParameter("quantity"));
		
		
		List<Part> fileParts = request.getParts().stream().filter(part -> "file".equals(part.getName()) && part.getSize() > 0).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">
		GestionAnnonces newAnnonce = new GestionAnnonces();
		int photoNumber = 0;
		
	    for (Part filePart : fileParts) {
	        photoNumber++;
	        if (!numbPhotos(photoNumber)) {
	        	session.setAttribute("transferUser", user);
	        	request.setAttribute("TooMuchPhotos", limitPhotos);
	        	request.getRequestDispatcher("/WEB-INF/NewAnnonce.jsp").forward(request, response);
	        }
	    }
	    
	    
	    try {
			newAnnonce.createAnnonce(titre, type, niveau, isbn,
					maisonEdition, dateEdition, prixUnitaire, remise, quantite,
					 fileParts, user);
			request.getRequestDispatcher("/WEB-INF/userInterface.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("isbnUnique", e);
			request.getRequestDispatcher("/WEB-INF/errorLoginUnique.jsp").forward(request, response);
		}
	}
	
	/**
	 * Vérifie si le nombre de photos intégré n'est pas supérieur à 3
	 * @param photos
	 * @return
	 */
	public boolean numbPhotos(int photos) {
		boolean isLimitOk = true;
		if(photos>3) {
			isLimitOk=false;
		}
		return isLimitOk;
	}
		
		
	}
