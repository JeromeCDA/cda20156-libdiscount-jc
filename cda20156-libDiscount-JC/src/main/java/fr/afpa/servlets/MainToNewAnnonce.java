package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;

/**
 * Servlet implementation class MainToNewAnnonce
 */
public class MainToNewAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("transferUser");
		session.setAttribute("transferUser", user);
		request.getRequestDispatcher("/WEB-INF/NewAnnonce.jsp").forward(request, response);
	}


}
