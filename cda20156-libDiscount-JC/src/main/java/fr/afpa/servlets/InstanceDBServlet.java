package fr.afpa.servlets;

import java.io.IOException; 

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.session.HibernateUtils;

/**
 * Servlet implementation class InstanceDBServlet
 */
public class InstanceDBServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		
	}

}
