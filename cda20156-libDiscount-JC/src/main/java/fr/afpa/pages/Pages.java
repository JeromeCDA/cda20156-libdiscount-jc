package fr.afpa.pages;

public interface Pages {
	
	public final static String LOGIN = "login.jsp";
	public final static String CREATEACCOUNT = "creationcompte.jsp";
	public final static String USERINTERFACE = "userInterface.jsp";
	public final static String ERRORLOGINUNIQUE = "errorLoginUnique.jsp";
	public final static String SERVLETNEWANNONCE = "NewAnnonce";
	public final static String NEWANNONCE = "NewAnnonce.jsp";
	public final static String MAINTONANNONCE = "MainToNewAnnonce";
}
