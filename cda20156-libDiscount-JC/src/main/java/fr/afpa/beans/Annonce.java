package fr.afpa.beans;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="Annonces")
public class Annonce {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="annonces_id_gen")
	@SequenceGenerator(name="annonces_id_gen", sequenceName="sequ_annonces", allocationSize=1)
	@Column(name="idAnnonce")
	private int idAnnonce;
	@Column(name="Titre")
	private String titre;
	@Column(name="Type")
	private String type;
	@Column(name="Niveau")
	private String niveau;
	@Column(name="ISBN", unique = true)
	private String isbn;
	@Column(name="Maison_Edition")
	private String maisonEdition;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_Edition", columnDefinition="DATE")
	private Date dateEdition;
	@Column(name="Prix_Unitaire")
	private double prixUnitaire;
	@Column(name="Remise")
	private int remise;
	@Column(name="Quantite")
	private int quantite;
	@Column(name="Prix_total")
	private double prixTotal;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="annonce")
	private List<Photo> listPhotos;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idUser")
	private User user;
	
	
	

	
	public Annonce(String titre, String type, String niveau, String isbn,
			String maisonEdition, Date dateEdition, double prixUnitaire, int remise, int quantite,
			double prixTotal, User user) {
		super();
		this.titre = titre;
		this.type = type;
		this.niveau = niveau;
		this.isbn = isbn;
		this.maisonEdition = maisonEdition;
		this.dateEdition = dateEdition;
		this.prixUnitaire = prixUnitaire;
		this.remise = remise;
		this.quantite = quantite;
		this.prixTotal = prixTotal;
		this.user = user;
	}
	

	@Override
	public String toString() {
		return this.titre+this.type+this.niveau+this.isbn+this.maisonEdition+this.dateEdition+this.prixUnitaire+
				this.remise+this.quantite+this.prixTotal;
	}

}