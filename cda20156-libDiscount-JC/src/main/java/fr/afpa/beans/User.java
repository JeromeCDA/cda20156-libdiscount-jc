package fr.afpa.beans;

import java.util.List; 

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="Utilisateur")
@NamedQuery(name = "findLoginPassword", query = "select user from Utilisateur user where user.login = :login and user.password = :password") 
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id_gen")
	@SequenceGenerator(name="user_id_gen", sequenceName="sequ_user", allocationSize=1)
	@Column(name="idUser")
	private int idUser;
	@Column(name="Nom")
	private String name;
	@Column(name="Prenom")
	private String firstName;
	@Column(name="useremail", unique = true)
	private String email;
	@Column(name="telephone", unique = true)
	@Size(min = 10, max = 10)
	private String telephone;
	@Column(name="login", unique = true)
	private String login;
	@Column(name="motdepasse")
	@Size(min=6)
	private String password;
	@Column(name="userrole")
	private int role;
	@Column(name="isActive")
	private boolean isActive;
	@OneToMany(cascade = CascadeType.ALL, mappedBy="user")
	private List<Annonce> listAnnonces;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idLibrairie")
	private Librairie librairie;

	
	
	public User(String name, String firstName, String email, String telephone, String login,
			String password, int role, boolean isActive) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.email = email;
		this.telephone = telephone;
		this.login = login;
		this.password = password;
		this.role = role;
		this.isActive = isActive;
	}


	@Override
	public String toString() {
		return this.firstName+this.name+this.email+this.telephone+this.login+this.password;
	}


}