package fr.afpa.beans;

import java.sql.Blob;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="Photos")
public class Photo {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="id_photos_gen")
	@SequenceGenerator(name="id_photos_gen", sequenceName="sequ_photos", allocationSize=1)
	@Column(name="idPhoto")
	private int idPhoto;
	@Column(name="Photo")
	@Lob
	private byte[] photo;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idAnnonce")
	private Annonce annonce;
	
	
	
	public Photo(byte[] photo) {
		super();
		this.photo = photo;
	}
	
	

	
}