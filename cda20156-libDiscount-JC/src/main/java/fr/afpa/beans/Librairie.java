package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="Librairie")
public class Librairie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="library_id_gen")
	@SequenceGenerator(name="library_id_gen", sequenceName="sequ_library", allocationSize=1)
	@Column(name="idLibrairie")
	private int idLibrairie;
	@Column(name="Nom_librairie", nullable = false)
	private String nomLibrairie;
	@Column(name="Adresse_librairie", nullable = false)
	private String voieAdresseLib;
	@Column(name="Code_postal", nullable = false)
	@Size(min = 5, max = 5)
	private String codePostalLib;
	@Column(name="Ville", nullable = false)
	private String villeLib;
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "librairie")
	private List<User> listUsers;
	

	public Librairie(String nomLibrairie, String voieAdresseLib, String codePostalLib,
			String villeLib) {
		super();
		this.nomLibrairie = nomLibrairie;
		this.voieAdresseLib = voieAdresseLib;
		this.codePostalLib = codePostalLib;
		this.villeLib = villeLib;
	}
	
	@Override
	public String toString() {
		return this.nomLibrairie+this.voieAdresseLib+this.codePostalLib+this.villeLib;
	}
	

}
