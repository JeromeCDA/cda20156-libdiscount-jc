package fr.afpa.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import fr.afpa.pages.Pages;

/**
 * Servlet Filter implementation class FiltreSession
 */
public class FiltreSession implements Filter {

    /**
     * Default constructor. 
     */
    public FiltreSession() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		
		if(req.getSession()!=null || req.getServletPath().equals("CreateAccountServlet") || req.getServletPath().equals("InstanceDBServlet") || req.getServletPath().equals("LoginServlet")  ) {
			chain.doFilter(request, response);			
		} else {
			request.getRequestDispatcher("/").forward(request, response);
		}
	}
	

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
